#!/bin/bash
#
# Init script for JIRA
#
# chkconfig: 123456 1 99
# description: JIRA init script
#
# Author: Sean McArdle
#
### BEGIN INIT INFO
# Provides: JIRA
# Required-Start: $local_fs $network mysql
# Required-Stop: $local_fs $network mysql
# Default-Start:  3 5
# Default-Stop:  0 1 2 6
# Short-Description: JIRA issue tracker
# Description: Web based project/issue tracking
### END INIT INFO

# include lsb init script functions inline
. /lib/lsb/init-functions

APP_NAME='JIRA'
APP_DIR='/data/jira_main/jira-std/'
APP_START="$APP_DIR/bin/start-jira.sh"
APP_STOP="$APP_DIR/bin/stop-jira.sh"
PID_PATH="$APP_DIR/work/catalina.pid"



start()
{
	
}

stop()
{
	
}

restart()
{
	stop
	start
}

status()
{
	
}

sanity_checks()
{
	# Check for init script
	if [ ! -r $APP_START ]; then
		log_warning_msg "$0: WARNING: $APP_START cannot be read"
		echo            "$0: WARNING: $APP_START cannot be read"
	fi
	
	# Check for config file
	local SETENV=$(dirname $APP_START)/setenv.sh
	if [ ! -r $SETENV  ]; then
		log_warning_msg "$0: WARNING: $SETENV cannot be read"
		echo            "$0: WARNING: $SETENV cannot be read"	
	fi
}


case "${1:-''}" in
	'start')
		
		;;

	'stop')

		;;

	'restart')

		;;

	'status')

		;;

	*)
		echo "Usage: $SELF start|stop|restart|status"
		exit 1
		;;
esac
